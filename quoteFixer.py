#! /opt/local/bin/python

#this program replaces the text on the clipboard with a programming friendly version
#specifically pretty quotes from pages, word, some crappy website... are replaced with the plain old ' and " for programming
#usage: 
#1 copy the offending text
#2 run this program
#3 paste into your programming text editor

import subprocess

#Thanks to David Foster for the below functions: http://stackoverflow.com/questions/1825692/can-python-send-text-to-the-mac-clipboard
def read_from_clipboard():
    return subprocess.check_output(
        'pbpaste', env={'LANG': 'en_US.UTF-8'}).decode('utf-8')
        
def write_to_clipboard(output):
    process = subprocess.Popen('pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
    process.communicate(output.encode('utf-8'))

testingString = read_from_clipboard()

#single quotes:
fixedString = testingString.replace(u'\u2018', "'")
fixedString = testingString.replace(u'\u2019', "'")

#double qutoes:
fixedString = fixedString.replace(u'\u201C', '"')
fixedString = fixedString.replace(u'\u201D', '"')

#zero width no-break space
fixedString = fixedString.replace(u'\uFEFF', '')

write_to_clipboard(fixedString)


#print testingString + ' changed to ' + fixedString + '\n'
